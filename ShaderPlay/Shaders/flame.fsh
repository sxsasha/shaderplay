//uniform vec3      iResolution;           // viewport resolution (in pixels) iResolution
//uniform float     iTime;                 // shader playback time (in seconds) u_time
//uniform float     iTimeDelta;            // render time (in seconds) -------------
//uniform int       iFrame;                // shader playback frame -------------
//uniform float     iChannelTime[4];       // channel playback time (in seconds) ------------
//uniform vec3      iChannelResolution[4]; // channel resolution (in pixels) -------------
//uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click ----------
//uniform samplerXX iChannel0..3;          // input channel. XX = 2D/Cube
//uniform vec4      iDate;                 // (year, month, day, time in seconds) --------------
//uniform float     iSampleRate;           // sound sample rate (i.e., 44100) ----------------
//fragColor //gl_FragColor
//fragCoord //gl_FragCoord
//void mainImage( out vec4 fragColor, in vec2 fragCoord ) //void main(void)

float noise(vec3 p) //Thx to Las^Mercury
{
    vec3 i = floor(p);
    vec4 a = dot(i, vec3(1., 57., 21.)) + vec4(0., 57., 21., 78.);
    vec3 f = cos((p-i)*acos(-1.))*(-.5)+.5;
    a = mix(sin(cos(a)*a),sin(cos(1.+a)*(1.+a)), f.x);
    a.xy = mix(a.xz, a.yw, f.y);
    return mix(a.x, a.y, f.z);
}

float sphere(vec3 p, vec4 spr)
{
    return length(spr.xyz-p) - spr.w;
}

float flame(vec3 p, float time)
{
    float d = sphere(p*vec3(1.,.5,1.), vec4(.0,-1.,.0,1.));
    return d + (noise(p+vec3(.0,time*2.,.0)) + noise(p*3.)*.5)*.25*(p.y) ;
}

float scene(vec3 p, float time)
{
    return min(100.-length(p) , abs(flame(p, time)) );
}

vec4 raymarch(vec3 org, vec3 dir, float time)
{
    float d = 0.0, glow = 0.0, eps = 0.02;
    vec3  p = org;
    bool glowed = false;
    
    for(int i=0; i<64; i++)
    {
        d = scene(p, time) + eps;
        p += d * dir;
        if( d>eps )
        {
            if(flame(p, time) < .0)
                glowed=true;
            if(glowed)
                glow = float(i)/64.;
        }
    }
    return vec4(p,glow);
}

void main(void)
{
    vec3 ff = u_time;
    
    vec2 v = -1.0 + 2.0 * gl_FragCoord.xy / iResolution.xy;
    v.x *= iResolution.x/iResolution.y;
    
    vec3 org = vec3(0., -2., 4.);
    vec3 dir = normalize(vec3(v.x*1.6, -v.y, -1.5));
    
    vec4 p = raymarch(org, dir, u_time);
    float glow = p.w;
    
    vec4 col = mix(vec4(1.,.5,.1,1.), vec4(0.1,.5,1.,1.), p.y*.02+.4);
    
    gl_FragColor = mix(vec4(0.), col, pow(glow*2.,4.));
    //fragColor = mix(vec4(1.), mix(vec4(1.,.5,.1,1.),vec4(0.1,.5,1.,1.),p.y*.02+.4), pow(glow*2.,4.));
    
}
