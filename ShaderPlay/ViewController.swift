//
//  ViewController.swift
//  ShaderPlay
//
//  Created by sxsasha on 31.03.18.
//  Copyright © 2018 sxsasha. All rights reserved.
//

import UIKit
import SpriteKit

class ViewController: UIViewController {
    
    @IBOutlet var spriteKitView: SKView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load the SKScene from 'GameScene.sks'
        let scene = SKScene(size: spriteKitView.bounds.size)
        scene.scaleMode = .aspectFill
        scene.backgroundColor = .black

        let uniformBasedShader = SKShader(fileNamed: "flame.fsh")

        let spriteSize = vector_float3(
            Float(spriteKitView.frame.size.width),  // x
            Float(spriteKitView.frame.size.height), // y
            Float(0.0)                       // z - never used
        )

        uniformBasedShader.uniforms = [
            SKUniform(name: "iResolution", vectorFloat3: spriteSize)
        ]

        let sprite = SKSpriteNode()
        //sprite.color = .green
        sprite.position = CGPoint(x: scene.size.width / 2, y: scene.size.height / 2)
        sprite.size = scene.size// CGSize(width: 50, height: 50)
        sprite.shader = uniformBasedShader

        scene.addChild(sprite)
        
        // Present the scene
        spriteKitView.presentScene(scene)
        
        spriteKitView.ignoresSiblingOrder = true
        
        spriteKitView.showsFPS = true
        spriteKitView.showsNodeCount = true
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

